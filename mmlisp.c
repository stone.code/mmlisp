/*
** This code is distributed under a Do What The F**k You Want To Public 
** License (WTFPL).  Please see LICENSE.txt.
**
*************************************************************************
**
** Copyright (c) 2010 Robert W. Johnstone
** Code based on "A micro-manual for LISP Implemented in C," located at
** nakkaya.com.
**
*************************************************************************
**
** This file contains the entire implementation of the mmlisp system.
** Users should include this file as part of their source, and they will
** have embedded LISP.
**
*/

#include "mmlisp.h"
#include <assert.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

/******************************************************************************
/* Memory Tracking
*/

#ifdef MMLISP_TRACK_MEMORY

unsigned mmlisp_objects = 0;

#define ADD_REFCOUNT(x) { (x)->refcount+=1; printf( "\t\tADD_REFCOUNT %p %u @%d\n", (x), (x)->refcount, __LINE__ ); }
#define RELEASE(x) { if (x) printf("\t\tRELEASE %p %u @%d\n", (x), (x)->refcount, __LINE__ ); release(x); };

#else

#define ADD_REFCOUNT(x) { (x)->refcount+=1; }
#define RELEASE(x) { release( x ); };

#endif

/******************************************************************************
/* Data Types
*/

enum type {CONS = 0xfa, ATOM = 0xfb, FUNC = 0xfc, LAMBDA = 0xfd};

struct mmlisp_object_tag {
	enum type type;
	unsigned refcount;
};

typedef struct mmlisp_object_tag object;

typedef struct {
	enum type type;
	unsigned refcount;
	char name[1];
} atom_object;

typedef struct {
	enum type type;
	unsigned refcount;
	object *car;
	object *cdr;
} cons_object;

typedef struct {
	enum type type;
	unsigned refcount;
	object* (*fn)(object*,object*);
} func_object;

typedef struct {
	enum type type;
	unsigned refcount;
	object* args;
	object* sexp;
} lambda_object;

static object *tee = 0;

/******************************************************************************
/* Simple access
/* Non refcounting
*/

char const * name(object *o)
{
	if ( !o )
		return "";
	if ( o->type == ATOM )
		return ((atom_object*)o)->name;
	else 
		return (char const*)o + sizeof(struct mmlisp_object_tag);
}

static int type(object* o )
{
	if ( o ) return o->type;
	return 0;
}

static object* car( object* o )
{
	assert( o->type == CONS );
	return ((cons_object*)o)->car;
}

static object* cdr( object* o )
{
	assert( o->type == CONS );
	return ((cons_object*)o)->cdr;
}

/******************************************************************************
/* Construction & release
*/

static object* atom(char const *n)
{
	atom_object *ptr = (atom_object *) malloc( sizeof(atom_object) + strlen(n) );
	assert( ptr );
	#ifdef MMLISP_TRACK_MEMORY
	mmlisp_objects++;
	#endif

	ptr->type = ATOM;
	ptr->refcount = 1;
	strcpy( ptr->name, n );
	#ifdef MMLISP_TRACK_MEMORY
	printf( "-->atom %p %s\n", ptr, ptr->name );
	#endif
	return (object *)ptr;
}

static object *cons (object *first, object *second)
{
	cons_object *ptr = (cons_object *) malloc (sizeof (cons_object));
	assert( ptr );
	#ifdef MMLISP_TRACK_MEMORY
	mmlisp_objects++;
	#endif

	ptr->type = CONS;
	ptr->refcount = 1;
	ptr->car = first;
	if ( first ) ADD_REFCOUNT( first );
	ptr->cdr = second;
	if ( second ) ADD_REFCOUNT( second );
	#ifdef MMLISP_TRACK_MEMORY
	printf( "-->cons %p (%p %p)\n", ptr, first, second );
	#endif
	return (object *) ptr;
}

static object *func (object* (*fn)(object*, object*))
{
	func_object *ptr = (func_object *) malloc (sizeof (func_object));
	assert( ptr );
	#ifdef MMLISP_TRACK_MEMORY
	mmlisp_objects++;
	#endif

	ptr->type = FUNC;
	ptr->refcount = 1;
	ptr->fn = fn;
	return (object *) ptr;
}

static void append (object *list, object *obj) 
{
	object *ptr;

	assert( list->type == CONS );

	// locate end of list
	for (ptr = list; cdr(ptr) != NULL; ptr = cdr(ptr));
	assert( ptr->type == CONS );

	assert( ((cons_object*)ptr)->cdr == 0 );
	((cons_object*)ptr)->cdr = cons(obj, NULL);
	assert( ((cons_object*)ptr)->cdr->refcount == 1 );
}

static object *lambda (object *args, object *sexp)
{
	lambda_object *ptr = (lambda_object *) malloc (sizeof (lambda_object));
	assert( ptr );
	#ifdef MMLISP_TRACK_MEMORY
	mmlisp_objects++;
	#endif

	ptr->type = LAMBDA;
	ptr->refcount = 1;
	ptr->args = args;
	if ( args ) ADD_REFCOUNT( args );
	ptr->sexp = sexp;
	if ( sexp ) ADD_REFCOUNT( sexp );
	return (object *) ptr;
}

static void release( object* o )
{
	if ( !o )
		return;

	--o->refcount;
	if ( o->refcount > 0 ) return;

	// Release the item
	switch ( o->type ) {
	case CONS:
		#ifdef MMLISP_TRACK_MEMORY
		printf( "<--cons %p (%p %p)\n", o, car(o), cdr(o) );
		#endif
		if ( ((cons_object *)o)->car ) RELEASE( ((cons_object *)o)->car );
		if ( ((cons_object *)o)->cdr ) RELEASE( ((cons_object *)o)->cdr );
		break;

	case ATOM:
		#ifdef MMLISP_TRACK_MEMORY
		printf( "<--atom %p %s\n", o, name(o) );
		#endif
		break;

	case FUNC:
		#ifdef MMLISP_TRACK_MEMORY
		printf( "<--func %p\n", o );
		#endif
		break;

	case LAMBDA:
		#ifdef MMLISP_TRACK_MEMORY
		printf( "<--lambda %p\n", o );
		#endif
		if ( ((lambda_object *)o)->args ) RELEASE( ((lambda_object *)o)->args );
		if ( ((lambda_object *)o)->sexp ) RELEASE( ((lambda_object *)o)->sexp );
		break;

	default:
		assert( 0 );
	};

	assert( o );
	#ifdef MMLISP_TRACK_MEMORY
	mmlisp_objects--;
	#endif
	free( o );
}

void mmlisp_release( mmlisp_object* o )
{
	RELEASE( o );
}

/******************************************************************************
/* Functions for special forms
*/

static object *eval (object *sexp, object *env);

static object *fn_car (object *args, object *env) 
{
	if ( args && car(args) && car(args)->type==CONS && cdr(args)==NULL ) {
		object* ret = car( car( args ) );
		ADD_REFCOUNT( ret );
		return ret;
	}
	else if ( !args )
		printf( "Error:  CAR used without arguments\n" );
	else if ( !car(args) )
		printf( "Error:  CAR used with an argument that is en empty list\n" );
	else if ( car(args)->type!=CONS )
		printf( "Error:  CAR used with an argument that is not a list\n" );
	else if ( cdr(args) )
		printf( "Error:  CAR used with more than one argument\n" );

	return NULL;
}

static object *fn_cdr (object *args, object *env) 
{
	if ( args && car(args) && car(args)->type==CONS && cdr(args)==NULL ) {
		object* ret = cdr( car(args) );
		ADD_REFCOUNT( ret );
		return ret;
	}
	else if ( !args )
		printf( "Error:  CDR used without arguments\n" );
	else if ( !car(args) )
		printf( "Error:  CDR used with an argument that is en empty list\n" );
	else if ( car(args)->type!=CONS )
		printf( "Error:  CDR used with an argument that is not a list\n" );
	else if ( cdr(args) )
		printf( "Error:  CDR used with more than one argument\n" );

	return NULL;
}

static object *fn_quote (object *args, object *env)
{
	if (args && cdr(args)==NULL ) {
		object* ret = car( args );
		if ( ret ) ADD_REFCOUNT( ret );
		return ret;
	}
	else if ( !args )
		printf( "Error:  QUOTE used without arguments\n" );
	else if ( cdr(args) )
		printf( "Error:  QUOTE used with more than one argument\n" );

	return NULL;
}

static object *fn_cons (object *args, object *env)
{
	if ( args && cdr(args) && (!car(cdr(args)) || car(cdr(args))->type==CONS) && cdr(cdr(args))==0 ) {
		object* list = cons(car(args), NULL );
		args = car( cdr(args) );

		while (args != NULL && args->type == CONS){
			append(list,car(args));
			args = cdr(args);
		}

		return list;
	}
	else if ( !args )
		printf( "Error:  CONS used without two parameters\n" );
	else if ( !cdr(args) )
		printf( "Error:  CONS used without two parameters\n" );
	else if ( cdr(args)->type!=CONS )
		printf( "Error:  Internal error\n" );
	else if ( car(cdr(args)) && car(cdr(args))->type!=CONS )
		printf( "Error:  CONS used with a second parameter that is not a list\n" );
	else if ( cdr(cdr(args)) )
		printf( "Error:  CONS used with more than three parameters\n" );

	return NULL;
}

static object *fn_equal (object *args, object *env)
{
	if ( args && cdr(args) && !cdr(cdr(args)) ) {
		object *first = car(args);
		object *second = car(cdr(args));

		if ( first==second ) {
			ADD_REFCOUNT( tee );
			return tee;
		}
		
		first = eval( first, env );
		second = eval( second, env );

		int ret = (first==NULL && second==NULL) || strcmp(name(first),name(second)) == 0;
		RELEASE( first );
		RELEASE( second );

		if ( ret ) {
			ADD_REFCOUNT( tee );
			return tee;
		}
		else 
			return NULL;
	}
	else if ( !args )
		printf( "Error:  EQUAL used without two parameters\n" );
	else if ( !cdr(args) )
		printf( "Error:  EQUAL used without two parameters\n" );
	else if ( cdr(cdr(args)) )
		printf( "Error:  EQUAL used with more than two parameters\n" );

	return NULL;
}

static object *fn_atom (object *args, object *env)
{
	if ( args && car(args) && car(args)->type==ATOM && cdr(args)==NULL ) {
		ADD_REFCOUNT( tee );
		return tee;
	}
	else if ( !args )
		printf( "Error:  ATOM used without a parameter\n" );
	else if ( cdr(args)!=NULL )
		printf( "Error:  ATOM used with more than one parameter\n" );

	return NULL;
}

static object *fn_nil (object *args, object *env)
{
	if ( !args ) {
		return NULL;
	}
	else if ( args )
		printf( "Error:  NIL used with a parameter\n" );

	return NULL;
}

static object *fn_cond (object *args, object *env)
{
	while (args != NULL && args->type == CONS) {
		object *list = car(args);
		if ( list ) {
			object *pred = eval(car(list), env);
			object *ret = car(cdr(list));

			if(pred != NULL) {
				RELEASE( pred );
				return eval(ret,env);
			}

			RELEASE( pred );
		}

		args = cdr(args);
	}

	return NULL;
}

/* Additions beyond paper */
static object *fn_length( object* args, object* env)
{
	if (args && cdr(args)==NULL && (!car(args) || car(args)->type==CONS) ) {
		unsigned count = 0;
		
		args = car(args);
		while ( args ) {
			++count;
			args = cdr(args);
		}

		char buffer[32];
		sprintf( buffer, "%u", count );
		
		return atom( buffer );
	}
	else if ( !args )
		printf( "Error:  LENGTH used without arguments\n" );
	else if ( cdr(args) )
		printf( "Error:  LENGTH used with more than one argument\n" );
	else if ( car(args) && car(args)->type!=CONS )
		printf( "Error:  LENGTH used with an argument that is not a list\n" );

	return NULL;
}

static object *interleave (object *c1, object *c2)
{
	object* tmp2 = cons( car(c2), NULL );
	object* tmp1 = cons( car(c1), tmp2 );
	RELEASE( tmp2 );
	object* list = cons( tmp1, NULL );
	RELEASE( tmp1 );
	c1 = cdr(c1);
	c2 = cdr(c2);

	while (c1 != NULL && c1->type == CONS){
		object* tmp2 = cons( car(c2), NULL );
		object* tmp1 = cons( car(c1), tmp2 );
		RELEASE( tmp2 );
		append(list,tmp1);
		RELEASE( tmp1 );
		c1 = cdr(c1);
		c2 = cdr(c2);
	}

	return list;
}

static object *replace_atom (object *sexp, object *with) {

  if(sexp->type == CONS){
	object* tmp = replace_atom( car(sexp), with );
	object* list = cons( tmp, NULL );
	RELEASE( tmp );

	sexp = cdr(sexp);
	while (sexp != NULL && sexp->type == CONS){
		object* tmp = replace_atom( car(sexp), with );
		append( list, tmp );
		RELEASE( tmp );
		sexp = cdr(sexp);
	}

    	return list;
	}
  else {
	object* tmp = with;

	while (tmp != NULL && tmp->type == CONS) {
		object *item = car(tmp);
		object *atom = car(item);
		object *replacement = car(cdr(item));

		assert( sexp );
		if ( strcmp(name(atom),name(sexp)) == 0) {
			ADD_REFCOUNT( replacement );
			return replacement;
		}

		tmp = cdr(tmp);
	}

	ADD_REFCOUNT( sexp );
    	return sexp;
  }
}

static object *fn_lambda (object *args, object *env)
{
	object *lambda = car(args);
	args = cdr(args);

	object *list = interleave((((lambda_object *) (lambda))->args),args);
	object* sexp = replace_atom((((lambda_object *) (lambda))->sexp),list);
	RELEASE( list );
	object* ret = eval(sexp,env);
	RELEASE( sexp );
	return ret;
}

object *fn_label (object *args, object *env)
{
	object* tmp_atom = atom(name(car(args)));
	object* tmp_tail = cons(car(cdr(args)),NULL);
	object* tmp_list = cons( tmp_atom, tmp_tail );
	RELEASE( tmp_atom );
	RELEASE( tmp_tail );
	append( env, tmp_list );
	RELEASE( tmp_list );

	ADD_REFCOUNT( tee );
	return tee;
}

/******************************************************************************
/* Eval
*/

object* lookup(char const* n, object* env)
{
	object *tmp = env;

	assert( n );
	assert( env );

	while (tmp != NULL && tmp->type == CONS) {
		object *item = car(tmp);
		assert( item );

		object *nm = car(item);
		object *val = car(cdr(item));

		assert( nm );
		if( strcmp(name(nm),n) == 0) {
			if (val) ADD_REFCOUNT( val );
			return val;
		}
		tmp = cdr(tmp);
	}

	return NULL;
}

object* mmlisp_init_env(){
	object *env = cons(cons(atom("QUOTE"),cons(func(&fn_quote),NULL)),NULL);
	append(env,cons(atom("CAR"),cons(func(&fn_car),NULL)));
	append(env,cons(atom("CDR"),cons(func(&fn_cdr),NULL)));
	append(env,cons(atom("CONS"),cons(func(&fn_cons),NULL)));
	append(env,cons(atom("EQUAL"),cons(func(&fn_equal),NULL)));
	append(env,cons(atom("ATOM"),cons(func(&fn_atom),NULL)));
	append(env,cons(atom("COND"),cons(func(&fn_cond),NULL)));
	append(env,cons(atom("LAMBDA"),cons(func(&fn_lambda),NULL)));
	append(env,cons(atom("LABEL"),cons(func(&fn_label),NULL)));
	append(env,cons(atom("LENGTH"),cons(func(&fn_length),NULL)));
	append(env,cons(atom("NIL"),cons(func(&fn_nil),NULL)));

	tee = atom("#T");

	return env;
}

static object *eval_fn (object *sexp, object *env)
{
	if ( sexp ) {
		object *symbol = car(sexp);

		if ( symbol==NULL ) 
			return NULL;
		assert( symbol );

		if(symbol->type == LAMBDA)
			return fn_lambda(sexp,env);

		if(symbol->type == FUNC)
			return (((func_object *) (symbol))->fn)(/*args*/cdr(sexp), env);

		/* return unaltered sexp */
		ADD_REFCOUNT( sexp );
		return sexp;
	}
	else
		return NULL;
}

static object *eval (object *sexp, object *env)
{
	if ( sexp == NULL )
		return NULL;

	if(sexp->type == CONS && strcmp(name(car(sexp)), "LAMBDA") == 0) {
		object* largs = car(cdr(sexp));
		object* lsexp = car(cdr(cdr(sexp)));

		return lambda(largs,lsexp);
	}
	else if(sexp->type == CONS) {
		object* tmp = eval( car(sexp), env );
		object *accum = cons(tmp,NULL);
		RELEASE( tmp ); tmp=0;
	    	sexp = cdr(sexp);

	    	while (sexp != NULL && sexp->type == CONS){
			object* tmp = eval(car(sexp),env);
	      		append( accum, tmp );
			RELEASE( tmp ); tmp = 0;
	      		sexp = cdr(sexp);
	    	}

	    	object* ret = eval_fn(accum,env);
		RELEASE( accum ); accum=0;
		/* transfer ownership ADD_REFCOUNT( ret );*/ 
		return ret;
	}
	else{
		assert( sexp );
		object *val = lookup(name(sexp),env);
		if(val == NULL) {
			ADD_REFCOUNT( sexp );
			return sexp;
		}
		else {
			ADD_REFCOUNT( val );
			return val;
		}
	}
}

object* mmlisp_eval( object *sexp, object *env) 
{
	return eval( sexp, env );
}

/******************************************************************************
/* File based IO
*/

void mmlisp_print(object *sexp)
{

	if(sexp == NULL) {
		printf( "()" );
		return;
	}

	if(sexp->type == CONS){
		printf ("(");
		mmlisp_print(car(sexp));
		sexp = cdr(sexp);
		while (sexp != NULL && sexp->type == CONS) {
			printf (" ");
			mmlisp_print(car(sexp));
			sexp = cdr(sexp);
		}
		printf ( ")");
	}
	else if(sexp->type == ATOM){
		printf ("%s", name(sexp));
	}
	else if(sexp->type == LAMBDA){
		printf ("#");
		mmlisp_print((((lambda_object *) (sexp))->args));
		mmlisp_print((((lambda_object *) (sexp))->sexp));
	}
	else
		printf ("Error.");
}

static object *next_token(FILE *in)
{
	int ch = getc(in);

	while(isspace(ch)) ch = getc(in);

	if(ch == '\n')
		ch = getc(in);
	if(ch == EOF)
		exit(0);

	if(ch == ')')
		return atom(")");
	if(ch == '(')
		return atom("(");

	char buffer[128];
	int index = 0;

	while(!isspace(ch) && ch != ')'){
		buffer[index++] = ch;
		ch = getc (in);
	}

	buffer[index++] = '\0';
	if (ch == ')') 
		ungetc (ch, in);

  	return atom(buffer);
}

static object *read_tail(FILE *in) {
	object *token = next_token(in);
	assert( token->refcount==1 );

	if(strcmp(name(token),")") == 0) {
		RELEASE( token );
    		return NULL;
	}
	else if(strcmp(name(token),"(") == 0) {
		RELEASE( token );
		object *first = read_tail(in);
		object *second = read_tail(in);
		object* ret= cons(first, second);
		RELEASE( first );
		RELEASE( second );
		return ret;
	}
	else {
		object *second = read_tail(in);
		object *ret = cons( token, second );
		RELEASE( token );
		RELEASE( second );
		return ret;
	}
}

object *mmlisp_read_file( FILE *in )
{
	object *token = next_token(in);

	if( strcmp(name(token),"(") == 0) {
		RELEASE( token );
		token = read_tail(in);
	}

	assert( !token || token->refcount == 1 );
	return token;
}

/******************************************************************************
/* String based IO
*/

static object *next_token_str( char const** in ) {
  int ch = /*getc*/ *(*in)++;

  while( isspace(ch)) ch = /*getc*/ *(*in)++;

  if(ch == '\n')
    ch = /*getc*/ *(*in)++;
  if(ch == '\0')
    exit(0);

  if(ch == ')')
    return atom(")");
  if(ch == '(')
    return atom("(");

  char buffer[128];
  int index = 0;

  while(!isspace(ch) && ch != ')'){
    buffer[index++] = ch;
    ch = /*getc*/ *(*in)++;
  }

  buffer[index++] = '\0';
  if (ch == ')') 
	/*ungetc*/ (*in)--;

  return atom(buffer);
}

static object *read_tail_str( char const** in )
{
	object *token = next_token_str(in);
	assert( token->refcount==1 );

	if( strcmp(name(token),")") == 0 ) {
		RELEASE( token );
		return NULL;
	}
	else if(strcmp(name(token),"(") == 0) {
		RELEASE( token );
		object *first = read_tail_str(in);
		object *second = read_tail_str(in);
		object * ret = cons( first, second );
		RELEASE( first );
		RELEASE( second );
		return ret;
	}
	else {
		object *second = read_tail_str(in);
		object * ret = cons( token, second );
		RELEASE( token );
		RELEASE( second );
		return ret;
	}
}

object* mmlisp_read_string( char const* in ) {
	object *token = next_token_str( &in );

	if(strcmp(name(token),"(") == 0)
		return read_tail_str( &in );

	return token;
}


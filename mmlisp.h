/*
** This code is distributed under a Do What The F**k You Want To Public 
** License (WTFPL).  Please see LICENSE.txt.
**
*************************************************************************
**
** Copyright (c) 2010 Robert W. Johnstone
** Code based on "A micro-manual for LISP Implemented in C," located at
** nakkaya.com.
**
*************************************************************************
**
** This file contains the public API of the mmlisp system.  Users should 
** be able to initialize the system, and execute lisp code.
**
*/

#ifndef _MMLISP_H_
#define _MMLISP_H_

#include <stdio.h>

struct mmlisp_object_tag;
typedef struct mmlisp_object_tag mmlisp_object;

#ifdef MMLISP_TRACK_MEMORY
extern unsigned mmlisp_objects;
#endif

mmlisp_object*	mmlisp_init_env( void );

mmlisp_object*	mmlisp_read_file( FILE* fp );
mmlisp_object*	mmlisp_read_string( char const* str );
mmlisp_object*	mmlisp_eval( mmlisp_object*, mmlisp_object* env );
void			mmlisp_print( mmlisp_object* );
void		mmlisp_release( mmlisp_object* );

#endif

This code is based off a LISP implementation published 2010-Aug-24 at the 
following URL:

http://nakkaya.com/2010/08/24/a-micro-manual-for-lisp-implemented-in-c/

The code was published without license information or copyright information,
so licensing of this code is ambiguous.  As for my portions, see LICENSE.txt.
Nakkaya code was, in turn, designed to implement a lisp dialect as described
in "A micro-manual for lisp - not the whole truth," by John McCarthy.

The code provided by Nakkaya has been (somewhat) cleaned up, and a public API
separated from the internal implementation.  The idea is that this is a very
minimal scripting solution.  Simply include mmlisp.c in the program.  The code
has been extended by increased error checking, and the implementation of 
reference counting for allocated objects.

A proper extension API has not yet been implemented.  However, users should be
able to implement extensions after reading the source.  For example, refer to
fn_cons, fn_car, and fn_quote.

Interested readers might also be interested in femtolisp, which is a stand-
alone LISP implementation, of considerable more sophistication.  In particular
there is a version in the repository for embedding.  This code could also be
extended (reference implementation of built-ins).

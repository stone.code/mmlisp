/*
** This code is distributed under a Do What The F**k You Want To Public 
** License (WTFPL).  Please see LICENSE.txt.
**
*************************************************************************
**
** Copyright (c) 2010 Robert W. Johnstone
** Code based on "A micro-manual for LISP Implemented in C," located at
** nakkaya.com.
**
*************************************************************************
**
** This file contains the glue code to read/write std io and interface
** with the mmlisp system.
**
*/

#include "mmlisp.h"
#include <assert.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{
	FILE* in = 0;

	mmlisp_object* env = mmlisp_init_env();
	#ifdef MMLISP_TRACK_MEMORY
	printf( "Objects = %u\n", mmlisp_objects );
	#endif

	if(argc > 1)
		in = fopen(argv[1], "r");
	else
		in = stdin;
	assert( in );

	do {
		printf ("> ");
		mmlisp_object* a = mmlisp_read_file(in);
		if ( a ) {
			mmlisp_object* b = mmlisp_eval( a, env );
			mmlisp_release( a );
			a = b;
		}
		mmlisp_print( a );
		printf ("\n");
		mmlisp_release( a );
		#ifdef MMLISP_TRACK_MEMORY
		printf( "Objects = %u\n", mmlisp_objects );
		#endif
	} while (1);

	printf( "\n" );
	return 0;
}
